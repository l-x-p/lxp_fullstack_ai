var str = 'hello world' //string类型
var n = 123 //number类型
var flag = false || true //Boolean类型
var u = undefined
var nu = null 

var obj = {
    a: 1,
    b: 'abc'
}
obj.c = 2 //对象obj新增一个 c


//数组
var arr = []
arr.push('hello') //尾部增加
arr.unshift(123) //首部增加
arr.pop() //尾部删除
arr.shift() //首部删除
console.log(arr);
