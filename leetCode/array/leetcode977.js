const nums = [-4,-1,0,3,10];

var sortedSquares = function(nums) {
    let n = nums.length;
    let result = Array(n).fill(0);
    for(i = 0, j = n - 1, k = n - 1; k >= 0; k--) {
        if(nums[i] * nums[i] <= nums[j] * nums[j]) {
            result[k] = nums[j] * nums[j];
            j--;
        }
        else {
            result[k] = nums[i] * nums[i];
            i++;
        }
    }
    return result;
};

console.log(sortedSquares(nums));