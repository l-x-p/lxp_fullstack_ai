// 移除元素

// let nums = [0,1,2,2,3,0,4,2], 
//     slow = 0; 

// 使用双指针快慢指针，fast指针用于遍历数组，将非val元素赋给slow,slow指针用于记录非val的元素,nums的长度不变
// var removeElement = function(nums, val) {
//     for(let fast = 0; fast < nums.length; fast++) {
//         if (nums[fast] != val) {
//             nums[slow++] = nums[fast];
//         }
//     }
//     return slow;
// };
// console.log(removeElement(nums, 2));
// console.log(nums.length);

// 使用splice()方法,splice删除元素，返回删除的元素，splice(i, 1)删除由i开始的1个元素 

let nums = [0,1,2,2,3,0,4,2];

var removeElement = function(nums, val) {
    for(let i = 0; i < nums.length; i++) {
        // if(nums[i] = val) {} 如果里面的式子打一个等号，相当于是赋值操作，
        // 任何非零数值都会返回true，所以if条件一直为true,就会一直执行splice(),删除所有的元素。
        if(nums[i] == val) {
            nums.splice(i, 1); //  不能写成 let removed = nums.splice(i, 1);因为removed 得到的是删除后的元素所组成的数组。
            i--;
        }  
    }
    return nums.length;
};

console.log(removeElement(nums, 2));
console.log(nums.length);
console.log(nums);
