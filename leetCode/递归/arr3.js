const arr = [1, 2, [3, 4, [5]]]

let str = arr.toString()

const newArr = str.split(',').map((item) => {
    return Number(item)
})

console.log(newArr);
