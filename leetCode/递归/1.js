// 阶乘 5! = 1 * 2 * 3 * 4 * 5
// function mul(n) {
//     let res = 1
//     for(let i = 1; i <= n; i++ ) {
//         res = res * i // res *= i
//     }
//     return res
// }

function mul(n) {
    if(n == 1) return 1

   return n * mul(n - 1)
}

// mul(5) = 5 * mul(4)
// mul(4) = 4 * mul(3)
// mul(3) = 3 * mul(2)
// mul(2) = 2 * mul(1)
// mul(1) = 1

// mul(n) = n * mul(n-1)

console.log(mul(10));