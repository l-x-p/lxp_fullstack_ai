// 斐波那契数列 1 1 2 3 5 8 13 21 34 55 89 ...

function fb(n) {
    if (n == 2 || n == 1) return 1
    return fb(n - 1) + fb(n - 2)
}

console.log(fb(10))

// fb(10) = fb(9) + fb(8)
// fb(n) = fb(n-1) + fb(n-2)