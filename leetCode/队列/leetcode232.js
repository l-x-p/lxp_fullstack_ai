var MyQueue = function() {
    this.stack1 = []
    this.stack2 = []
};
MyQueue.prototype.push = function(x) {
    this.stack1.push(x)
};
MyQueue.prototype.pop = function() {
    if(this.stack2.length == 0) {
         // 遍历第一个栈，将其中所有的元素导入栈2中
        while(this.stack1.length) {
            this.stack2.push(this.stack1.pop())
        }  
    }
   return this.stack2.pop()
};
MyQueue.prototype.peek = function() {
    if(this.stack2.length == 0) {
        // 遍历第一个栈，将其中所有的元素导入栈2中
       while(this.stack1.length) {
            this.stack2.push(this.stack1.pop())
       }  
   }
   const stack2Len = this.stack2.length
   return this.stack2[stack2Len - 1]
};
MyQueue.prototype.empty = function() {
    // if(this.stack1.length == 0 && this.stack2.length == 0) {
    //     return ture
    // }
    // return false
    return !this.stack1.length && !this.stack2.length
};

let queue = new MyQueue()
queue.push()
queue.pop()