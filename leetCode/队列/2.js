var nums = [1,3,-1,-3,5,3,6,7]
var k = 3

var maxSlidingWindow = function(nums, k) {
    const len = nums.length
    const res = []
    const queue = [] // 维护一个递减队列
    for (let i = 0; i < len; i++) {
        // 打破递减趋势
       while (queue.length && nums[queue[queue.length - 1]] <= nums[i]) {
            queue.pop()
       } 
       queue.push(i)
       // 需要有值流出窗口
       while(queue.length && queue[0] <= i - k) {
        queue.shift()
       }
       if(i >= k - 1) {
        res.push(nums[queue[0]])
       }
    }
    return res
}
console.log(maxSlidingWindow(nums, k));