var nums = [1,3,-1,-3,5,3,6,7]
var k = 3

var maxSlidingWindow = function(nums, k) {
    const len = nums.length
    const res = []
    let i = 0, j = k - 1;
    while(j < len) {
        const max = calMax(nums, i, j)
        res.push(max)
        i++
        j++
    }
    return res
};

function calMax(arr, i, j) {
    let max = -Infinity
    for(let m = i; m <= j; m++) {
        if(arr[m] > max) {
            max = arr[m]
        }
    }
    return max
}
console.log(maxSlidingWindow(nums, k));
 // [3,3,5,5,6,7]