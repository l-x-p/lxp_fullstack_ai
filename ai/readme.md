# 基于 LLM 开发 AI 应用

-   chatgpt
-   chat  bot 聊天机器人
    chatgpt   +   应用场景 （英语口语）    

-   AI时代开发应用很简单
    -   AI就在身边
    -   不只是聊天，练英语口语
        LLM大模型  可以了解 我们的需求（自然定义），返回回答
    -   开发者 传统  20人创业团队， 500w
    -   如何 加上纠错这个功能呢？
        I like sprint . It is my favorite season.
-   prompt engineering  提示词
    -  不需要coding  LLM Coding.
    -  如果我的拼写有错误，
    -  告诉 LLM prompt ? 提示大模型，这样为我工作
-   AIGC
    -  文字
    -  图片  AI体验
-   AI应用
    - 人设 角色  以职业来划分的
       精准
    - 需求？ 详细，可执行，一步步来做

# 腾讯高质量代码

-   面向对象
    - js 表现力是最强的 { }
    - 属性 和 方法 
-   代码要够健壮
      代码被怎么调用不清楚，要留个心眼
      receiver && receiver.receiveFlower()
-   抽象
      lxp  lt  对象越来越多的时候，复杂？
    - 形参  
-   接口 interface
      当多个对象具有相当的方法时，可以互换使用
-   代理模式 proxy
      面向对象设计模式的一种，可以通过接口让对象之间互换使用，从而达到某些复杂目的
-   Js语法
    - 简单数据类型 
        字符串String,数值类型NUmber,布尔值类型Boolean,Null,Undefined
    - 复杂数据类型
        其他的一切都是对象  Object  {} []  function也是对象
-   代码规范
    - 大厂都有代码规范，写代码前读一读
        - 驼峰式命名
        - 结尾以 ；结束
        - 多写注释
