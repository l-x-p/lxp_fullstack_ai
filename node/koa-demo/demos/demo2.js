const Koa = require('koa');
const app = new Koa();
const main = (ctx) => { // ctx是上下文对象
    if(ctx.request.header.accept == 'xml'){
        ctx.body = '<data>Hello World</data>'
    } else if(ctx.request.accepts('html')) {
        ctx.body = '<p>Hello World</p>'
    }
}
app.use(main)

app.listen(3000, () => {
    console.log('listening on port 3000');
})