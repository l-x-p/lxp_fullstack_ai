const Koa = require('koa');
const app = new Koa();
const main = (ctx) => { // ctx是上下文对象
    // console.log(ctx.request);
    // console.log(ctx.req);
    // ctx.res.end('hello')
    // ctx.response.body = 'hello world' //body是个属性，响应体
    // console.log(ctx.req.url);
    // console.log(ctx.request.url);
    console.log(ctx.url);
    ctx.body = 'hello Koa'
}
app.use(main)

app.listen(3000, () => {
    console.log('listening on port 3000');
})




// {
//     request: {
//       method: 'GET',
//       url: '/',
//       header: {
//         host: 'localhost:3000',
//         connection: 'keep-alive',
//         'cache-control': 'max-age=0',
//         'sec-ch-ua': '"Not/A)Brand";v="8", "Chromium";v="126", "Google Chrome";v="126"',
//         'sec-ch-ua-mobile': '?0',
//         'sec-ch-ua-platform': '"Windows"',
//         'upgrade-insecure-requests': '1',
//         'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/126.0.0.0 Safari/537.36',      accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
//         'sec-fetch-site': 'none',
//         'sec-fetch-mode': 'navigate',
//         'sec-fetch-user': '?1',
//         'sec-fetch-dest': 'document',
//         'accept-encoding': 'gzip, deflate, br, zstd',
//         'accept-language': 'zh-CN,zh;q=0.9'
//       }
//     },
//     response: {
//       status: 404,
//       message: 'Not Found',
//       header: [Object: null prototype] {}
//     },
//     app: { subdomainOffset: 2, proxy: false, env: 'development' },
//     originalUrl: '/',
//     req: '<original node req>',
//     res: '<original node res>',
//     socket: '<original node socket>'
//   }