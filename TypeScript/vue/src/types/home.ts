export type HomeTopBarItem = {
    /** 标题 */
    title: string
    /** 图标 */
    icon: string
  }


//  js 里没有 type, ts里有type,
//  使用 ts 关键字 自定义一个类型的约束
//  类型声明以 ; 隔开
  export type RecentlyViewedItem = {
    title: string;

    cover: string;
    
    price: number;
  }