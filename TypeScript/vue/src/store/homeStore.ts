import { defineStore } from "pinia";
import { ref } from "vue";
import type { HomeTopBarItem, RecentlyViewedItem } from "@/types/home";

export const useHomeStore = defineStore("home", {
  state: () => {
    const topBarState = ref<HomeTopBarItem[]>([
        {
          title: "游览 & 体验",
          icon: "photo-o",
        },
        {
          title: "景点门票",
          icon: "coupon-o",
        },
        {
          title: "酒店",
          icon: "hotel-o",
        },
        {
          title: "交通",
          icon: "logistics",
        },
        {
          title: "租车",
          icon: "guide-o",
        },
      ])

    // 泛型 navBar [] 每一项的类型
    // ref 类型约束 程序 更正确
    // 关键数据
    const navBarState = ref<HomeTopBarItem[]>([
        {
            title:'首页',
            icon: 'home-o'
        },
        {
            title: "目的地",
            icon: "location-o",
          },
          {
            title: "我的",
            icon: "manager-o",
          },
          {
            title: "喜欢",
            icon: "like-o",
          },
          {
            title: "更多",
            icon: "more-o",
          }      
    ])

    const recentlyViewedState = ref<RecentlyViewedItem[]>([
        {
            title: "曼谷",
            cover: "",
            price: 173
        },
        {
            title: "香港",
            cover: "",
            price: 213
        }
    ])
    return {
      topBarState,
      navBarState,
      recentlyViewedState
    };
  },
});