## HTML5 打击乐

-html5 让我们拥有强大的模仿能力，
    音乐键盘

- 相对单位
    css3 为了适配市面上的各种设备
    电脑、pad、各种phone的适配 鸿蒙
    vh 100vh 代表整体 按照百分比去拿
    vw 
    rem 会和 html 里的 font-size 等比例适配
    只要跟html一个元素进行等比例

    px 绝对单位
    移动端不适合

- background 考点
    - 盒子的大小
        body 宽和高是不定的
        图片 px 绝对大小
    - no-repeat 不会平铺
    - 如何铺满整个盒子？
    - background-position bottom center
    - 大大 等比例缩放 确保图片不失帧
    - contain 只在一个方向上填充满box，另一个方向空着，背景优先
    - cover 容器优先