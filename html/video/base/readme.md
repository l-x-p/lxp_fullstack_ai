# 块级元素 display:block
  1.默认占据一整行
  2.可以设置宽高

# 行内块级 display:inline-block
  1.不会默认占据一整行 
  2.可以设置宽高

# 行内元素
  1.不会默认占据一整行
  2.不可以设置宽高

# transform
  1.translate  平移
  2.rotate  旋转
  3.skew  倾斜
  4.scale  缩放