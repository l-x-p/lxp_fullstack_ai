//获取秒针
//设置秒针旋转的角度
//获取分针
//获取分针的旋转角度
//获取时针
//获取时针的旋转角度

var secondHand=document.querySelector('.second-hand');
var minHand=document.querySelector('.min-hand');
var hourHand=document.querySelector('.hour-hand');

function setTime(){
    var now=new Date();//获取当前时间
    
    //读取秒
    var seconds=now.getSeconds();
    var secondsDegrees=seconds*6+90;
    secondHand.style.transform=`rotate(${secondsDegrees}deg)`;/* ``用于变量和字符串拼接  */

    var mins=now.getMinutes();
    var minsDegrees=mins*6+90;
    minHand.style.transform=`rotate(${ minsDegrees}deg)`;

    var hours=now.getHours();
    var hoursDegrees=hours*30+90+(mins/60)*30;
    hourHand.style.transform=`rotate(${hoursDegrees}deg)`;
}

setTime();
//定时器
setInterval(setTime,1000);