class Parent {
    constructor(name) {
        this.name = 'Tom';
        this.like = [1, 2, 3]
    }

    getName() {
        return this.name;
    }
}

class Child extends Parent{
    constructor(name) {
        super(name);
        this.age = 20
    }
}

let c = new Child('Jerry')
console.log(c.getName());