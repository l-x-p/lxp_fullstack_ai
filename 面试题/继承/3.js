let Parent = {
    name: 'Tom',
    age: 40,
    like: [1, 2],
    getLike: function () {
        return this.like
    } 
}

let child = Object.create(Parent);
// let child2 = Object.create(Parent);
// child.like.push(3)

console.log(child.getLike());