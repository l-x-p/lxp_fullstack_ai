const get = createGetter()
const set = createSetter() 
 
function createGetter() {
    return function get (target, key, receiver) {
        console.log('正在读取值');
        return Reflect.get(target, key, receiver) // == target[key]
    }
}


function createSetter() {
    return function set(target, key, value, receiver) {
        return Reflect.set(target, key, value, receiver)
    }
}

export const mutableHandlers = {
    get,
    set
}