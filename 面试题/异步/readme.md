# 异步的发展

1. 回调
2. promise
    - 拥有三种状态 pending fulfilled rejected
    - 实例化一个 promise 会得到一个状态为 pending 的实例对象
    - 该对象可以访问 promise 原型上的 then 方法
    - 当该对象中的状态 没有 变更为 fulfilled, then接受到的回调是不触发的。
    - 当该对象中的状态 没有 变更为 rejected, catch接受到的回调是不触发的。
3. generator
- generator函数得分执行返回一个Generator对象，该对象包含一个next方法
- next 方法返回一个对象，包含两个属性 value done
4. async/await

