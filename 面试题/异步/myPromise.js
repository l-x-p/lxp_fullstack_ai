// class MyPromise {
//     constructor(executor) {
//         this.state = 'pending';
//         this.value = undefined; // 临时保存 resolve 的参数
//         this.reason = undefined; // 临时保存 reject 的参数
//         this.onFulfilledCallbacks = []; // 存放then的回调
//         this.onRejectedCallbacks = []; // 存放catch的回调
//         const resolve = (value) => {
//             if(this.state === 'pending') {
//                 this.state = 'fulfilled';
//                 this.value = value;
//                 this.onFulfilledCallbacks.forEach(cb => cb(value))
//             }
//         }
//         const reject = () => {
            
//         }
//         executor(resolve, reject)
//     }

//     then(onFulfilled, onRejected) {
//         onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : value => value;
//         onRejected = typeof onRejected === 'function' ? onRejected : reason => { throw reason };

//         // 将自己的回调存进onFulfilledCallbacks
//         return new MyPromise((resolve, reject) => {
//             if(this.state === 'fulfilled') { // then前面的那个哥们已经彻底执行完毕了
//                 setTimeout(() => { // 模拟异步，但是模拟不了微任务
//                     try{
//                         const res= onFulfilled(this.value)
//                         resolve(res)
//                     }catch (error) {
//                         reject(error)
//                     }
                    
//                 })
//             }

//             if(this.state === 'rejected') {
//                 setTimeout(() => { // 模拟异步，但是模拟不了微任务
//                     try{
//                         const res= onRejected(this.value)
//                         resolve(res)
//                     }catch (error) {
//                         reject(error)
//                     }
                    
//                 })
//             }

//             if(this.state === 'pending') {
//                  // 将自己的回调存进onFulfilledCallbacks
//                  this.onFulfilledCallbacks.push((value) => {
//                     setTimeout(() => {
//                         onFulfilled(value)
//                     })
//                  })
//                  this.onRejectedCallbacks.push((reason) => {
//                     setTimeout(() => {
//                         onRejected(reason)
//                     })
//                  })
//             }
//         })
//     }
// }

// new MyPromise((resolve, reject) => {
//     resolve()
// })
// .then(res => {

// })

class MyPromise {
    constructor(executor) { // 将传进来的函数体用executor执行掉
        this.state = 'pending';
        this.value = undefined;  // 临时保存 resolve 
        this.reason = undefined; // 临时保存 reject 的参数
        this.onFulfilledCallbacks = []; // 装then中的回调
        this.onRejectedCallbacks = []; // 装catch中的回调

        const resolve = (value) => {
            if(this.state === 'pending') {
                this.state = 'fulfilled';
                this.value = value;
                this.onFulfilledCallbacks.forEach(cb => cb(value))
            }
        }

        const reject = (reason) => {
            if(this.state === 'pending') {
                this.state = 'rejected';
                this.reason = reason;
                this.onRejectedCallbacks.forEach(cb => cb(reason))
            }
        }

        executor(resolve, reject)

    }

    then(onFulfilled, onRejected) {
        onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : value => value;
        onRejected = typeof onRejected === 'function' ? onRejected : reason => {throw reason};

        // 将自己的回调存进 onFulfilledCallbacks，让resolve来触发回调
        return new MyPromise((resolve, reject) => {
            if() {
                
            }
        })
    }
}

new MyPromise(() => {})

.then(
    (res) => {
        console.log(res);
    },
    (err) => {
        console.log(err);
    }
)