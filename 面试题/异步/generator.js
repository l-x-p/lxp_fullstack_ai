// function A() {
//     return newPromise(function (resolve, reject) {
//         setTimeout(() => {
//             console.log('异步a');
//             resolve()
//         }, 1000)
//     })
// }

// function B() {
//     console.log('同步b');
// }

function A() {
        setTimeout(() => {
            console.log('异步a');
        }, 1000)
}

function B() {
    console.log('同步b');
}

// A().then(B)

function* gen() {
    let a = yield A();
    console.log(a, '---');
    yield B();
}

let g = gen(); // new Generator();

console.log(g.next()); // {value: xxx, done: false}
// console.log(g.next()); // {value: undefined, done: false}
// console.log(g.next()); 