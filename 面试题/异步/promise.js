// const { log } = require("console");

// function a() {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             console.log('a is ok');
//             // resolve(1);
//             reject(1);
//         }, 1000)
//     })
// }

// function b() {
//     console.log('b is ok');
// };

// a().then((res) => {
//     console.log(res);
//     b();
// })

// //  a().then(b)
// .catch(err => {
//     console.log(err);
// })

const promise = new Promise((resolve, reject) => {
    console.log(1);
    setTimeout(() => {
      console.log("timerStart");
      resolve("success");
      console.log("timerEnd");
    }, 0);
    console.log(2);
  });
  promise.then((res) => {
    console.log(res);
  });
  console.log(4);