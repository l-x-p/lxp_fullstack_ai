// 延迟执行函数
function A(callback) {
    setTimeout(() => {
      console.log('异步a');
      callback()
    }, 1000)
  }
  function B(callback) {
    setTimeout(() => {
      console.log('异步b');
      callback()
    }, 500)
  }
  function C(callback) {
    setTimeout(() => {
      console.log('异步c');
      callback()
    }, 100)
  }
  
  
  // 延迟执行函数
  
  function simpleThunk(callback) {
    setTimeout(() => {
      const result = 42
      callback(result)
    }, 1000)
  }
  
  simpleThunk(function(res) {
    console.log(res); // 42
  })
