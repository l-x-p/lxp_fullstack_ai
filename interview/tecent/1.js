// 编译
console.log(a); // 变量提升 不太好， js是当初一周写出来的，有很多糟粕
// 代码的可读性
console.log(b); // 暂时性死区
// var a;
// a = 1;
var a = 1;
const b = 2;

// i 污染了全局作用域

for (var i = 0; i < 10; i++) {
    console.log(i);
}

for (let i = 0; i < 10; i++) {
    console.log(i);
}