// test()
// function test() {
//     var a = 123
//     console.log(a);

// }

// //声明提升
// function test() {
//     var a = 123
//     console.log(a);

// }
// test()

// GO: {
//     a: undefined    1
//     fn: function fn(a) {}
// }

var a = 1
function fn(a) {
    var a = 2
    function a() {}
    console.log(a);// 2
}

// AO:{
//     a: undefined     3   a: function() {}   2
// }
fn(3)



// GO: {
//     fn: function() {}
// }


// AO: {
//     a: undefined,  1,   function() {},  123,
//     b: undefined,       function() {},
//     c: undefined,      function c() {},   123


// }