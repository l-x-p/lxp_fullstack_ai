var global = 100

function fn() {
    console.log(global)
}
fn()

// GO {
//     global: undefined, 100
//     fn: function() {}
// }

// AO {

// }

// GO {
//     golb: undefined
//     a: function
// }