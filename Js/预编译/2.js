function a() {
    function b() {
        var b = 22
    }
    var a = 111
    b()
    console.log(a);
}
var glob = 100
a()

// 函数 a 的定义 a.[[scope]]  ---> 0: GO{}   // 在 a 的作用域中一定可以访问GO
// 函数 a 的执行 a.[[scope]]  ---> 0: aAO{}  1: GO{} 

// 作用域链