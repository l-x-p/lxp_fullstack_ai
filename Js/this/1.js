let obj = {
    myname: 'litian',
    age: 18,
    bar: function() { // 在对象内部方法中使用对象内部的属性
        console.log(this.myname);
    }
}
obj.bar()

function foo() {
    let name = 'lxp'
    console.log(name);
}

foo() 