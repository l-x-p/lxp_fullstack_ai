// function foo() {

// }

// var foo = function() {
//     var bar = () => { // 箭头函数不承认this,写在箭头函数里的this,是外层非箭头函数foo函数的
//         console.log(this);
//     }
//     bar()
// }

// foo()

var obj = {
    a: 1,
    b: function() {
        const fn = () => {
            console.log(this.a);
        }
        fn()
    }
}
obj.b()

