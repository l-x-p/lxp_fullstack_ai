# 显示类型转换
1. 原始值转布尔 Boolean()
2. 原始值转数字 Number()
3. 原始值转字符串 String()
4. 原始值转对象 new Xxx()


# 对象转原始值 -- 通常发生隐式转换
1. 任何对象转布尔,都是true
2. 对象转字符串 
    1. ToString(x)
    2. ToPrimitive(x)
3. 对象转数字  
    1. ToNumber(x)
    2. ToPrimitive(x)

所有对象转原始值都会调用toString()

1. {}.toString() 得到由"[object 和 class 和 ]"组成的字符串
2. [].toString() 返回由数组内部元素以逗号拼接的字符串
3. xx.toString() 返回字符串字面量 

- valueOf() 也可以将对象转成原始类型
1. 包装类对象

- 一元运算符 +

# ToPrimitive()
ToPrimitive(obj, String) => String(obj)
1. 如果接收到的是原始值，直接返回值
2. 否则，调用toString方法，如果得到原始值，返回值
3. 否则，调用valueOf方法，如果得到原始值，返回值
4. 否则，报错


ToPrimitive(obj, Number) => Number(obj)
1. 如果接收到的是原始值，直接返回值
2. 否则，调用valueOf方法，如果得到原始值，返回值
3. 否则，调用toString方法，如果得到原始值，返回值
4. 否则，报错


- 对象转布尔，一定是true


# 一元操作符 +
4.js

# 二元操作符 +

5.js

1. lprim = ToPrimitive(v1)
2. rprim = ToPrimitive(v2)
3. 如果 lprim 或者 rprim 是字符串，那么就ToString(lprim)或者ToString(rprim) 再拼接
4. 否则，那么就ToNumber(lprim) + ToNumber(rprim)

# ==  11.9.3

1 == {}
