// 1 == {}
// 1 == ToPrimitive({})
// 1 == '[object Object]'
// 1 == ToNumber('[object Object]')
// 1 == NaN

// [] == ![] 
// [] == false
// [] == ToNumber(false)
// [] == 0
// ToPrimitive([]) == 0
// '' == 0
// ToString('') == 0
// 0 == 0