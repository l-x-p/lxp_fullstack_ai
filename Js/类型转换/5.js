// 1 + '1' => '11'
// 1. ToPrimitive(1)  ToPrimitive('1')
// 1  '1'
//ToString(1) + '1'  
// '1' + '1' => '11'

// null + 1
// ToPrimitive(null) + ToPrimitive(1)
// null + 1
// ToNumber(null) + ToNumber(1)
// 0 + 1

// [] + {}
// ToPrimitive([]) + ToPrimitive({})
// [].valueof() + {}.valueof()
// [].toString() + {}.toString()
// '' + '[object Object]'
// [object Object]
