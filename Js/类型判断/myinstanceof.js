function myinstanceof(left, right) {
    while(left !== null) {
        if(left.__proto__ == right.prototype) {
            return true
        }
        left = left.__proto__
    }
    return false
}
console.log(myinstanceof([], Array)); // true
console.log(myinstanceof([], Object)); // true
console.log(myinstanceof({}, Array)); // false
