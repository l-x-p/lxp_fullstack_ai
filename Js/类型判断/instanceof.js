let s = '123'
let n = 123
let b = true
let u = undefined
let nul = null
let sy = Symbol(123)
let big = 999999999999999999999999999999999999999n


let obj = {}
let arr = []
let fn = function () {}
let date = new Date() // 日期对象

// console.log(obj instanceof Object);
// console.log(arr instanceof Array);
// console.log(fn instanceof Function);
// console.log(date instanceof Date);
// console.log(s instanceof String);
console.log(arr instanceof Object);
// arr.__proto__ = Array.prototype
// Array.prototype.__proto__ = Object.prototype