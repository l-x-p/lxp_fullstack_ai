# typeof
1. 可以判断除了null之外的所有原始类型
2. 除了function其他所有的引用类型都会被判断成object
3. typeof是通过将值转换为二进制后判断其二进制前三位是否为0，是则为object(对象转化为二进制后的前三位为0)

# instanceof
1. 只能判断引用类型
2. 是通过原型链查找来判断类型

# Object.prototype.toString(this)
1. 
2. 
3. 将 O 作为 ToObject(this) 的执行结果
4. 定义一个class作为内部属性 [[class]] 的值
5. 返回由 "[object " 和 class 和 "]" 组成的字符串

# Object.prototype.toString.call(xxx)

xxx.toString()

# Array.isArray(xxx)