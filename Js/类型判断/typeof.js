let s = '123'
let n = 123
let b = true
let u = undefined
let nul = null
let sy = Symbol(123)
let big = 999999999999999999999999999999999999999n


let obj = {}
let arr = []
let fn = function () {}
let date = new Date() // 日期对象
// console.log(typeof s);
// console.log(typeof(n));
// console.log(typeof nul); // object
// console.log(typeof b, typeof u, typeof nul, typeof sy, typeof big);
console.log(typeof obj, typeof arr, typeof fn, typeof date);

