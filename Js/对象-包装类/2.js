// var obj = {} // 创建对象字面量 / 对象直接量

// Object() 构造函数  String()  Number()  Boolean()  

// var obj2 = new Object() // new一个函数得到一个对象
// obj2.name = 'Tom'


// console.log(obj2);
// function foo() {

// }
// new foo()

function Car(color) {
    this.name = 'su7'
    this.height = '1400'
    this.lang = '5000'
    this.weight = 1000
    this.color = color
}

let car1 = new Car('red')
let car2 = new Car('green')

console.log(car1, car2);