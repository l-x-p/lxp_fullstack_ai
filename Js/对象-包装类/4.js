// 包装类

// 原始值是不能拥有属性和方法的。属性和方法是对象独有的

// var num = '123' // num = new Number(123)
// num.abc = 'hello' // Number(123).abc = 'hello'
// console.log(num.abc);

// v8眼中 (包装类的过程 -- 这个隐式的过程)
// new Number(123).abc = 'hello'
// delete new Number(123).abc
// console.log(num.abc);

// console.log(num.abc);

// var num = new Number(123) 
// num.abc = 'hello'
// console.log(num.abc);
// console.log(num * 2);




// var str = 'abcd'  // str = new String('abcd')
// console.log(str.length);