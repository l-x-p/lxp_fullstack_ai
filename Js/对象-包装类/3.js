// // 构造函数
// function People(name, age, sex) {
// //  var this = {
// //     age: ''
// //     name: ''
// //   }
//     this.name = name
//     this,age = age
//     this.sex = sex
//     // return this
// }

// let p1 = new People('long', 18, 'boy') //构造函数创建的对象叫作实例对象

// // 构造函数被new时的过程


// console.log(p1);


function Person(name, age) {
    var that = {}
    that.name = name
    that.age = age
    return that
}
let p1 = Person('yan', 18)
let p2 = Person('tian', 20)

console.log(p1);
console.log(p2);