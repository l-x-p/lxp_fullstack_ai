// let arr = ['a', 'b', 'c', 'd']
// let a = arr[0]
// console.log(a);
// let[a, b, c, d] = arr
// console.log(a, b, c, d);

// let arr = [1, 2, 3, 4, 5, 6]
// let [a, b, ...c] = arr
// let[a, b, c, d, e, f = 100] = arr
// console.log(a, b, c, d, e, f);

// let obj = {
//     a: 1,
//     b: 2,
//     c: {
//         n: 3
//     }
// }

// let foo = obj.a
// let bar = obj.c.n

// let { a, b, c, c: { n } } = obj
// console.log(a, b, c, n);

// let str = 'hello'
// let[a, b, c, d, e] = str
// console.log(a, b, c, d, e);