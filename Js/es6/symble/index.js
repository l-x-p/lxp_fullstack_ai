// var str = 'hello world';
// var str2 = 'hello world';

// var s = 1
// var s2 = '1'
// console.log(s == s2); // '==' v8会先把字符串转成数字再进行比较
// console.log(s === s2); // '===' v8不会发生类型转换

// var obj = {}
// var obj2 = {}

// console.log(obj == obj2);
// console.log(obj === obj2);// js中不存在两个相同的引用类型

var a = Symbol(123) // Symble 类型也是原始类型，创建一个独一无二的值
var b = Symbol(123)

console.log(a === b);