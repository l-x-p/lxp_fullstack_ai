function a() {
    return new Promise(function(resolve, reject){
        setTimeout(function() {
            console.log('a is ok');
            resolve('a')
        }, 1000)
    })
}

function b() {
    return new Promise(function(resolve, reject){
        setTimeout(function() {
            console.log('b is ok');
            resolve('b')
        }, 500)
    })
}

function c() {
    console.log('c is ok');
}

Promise.all([a(), b()]).then(() =>{
    c()
})
