// const arr = [1, 2, 3, 4, 4, 3, 2, 1];
// const arr2 = new Array(5)

// const s = new Set()
// s.add(1)
// s.add(2)
// s.add(2)

// console.log(s);

// const s = new Set([1, 2, 3, 3])  // set接收的参数具有iterable属性的结构
// console.log(s.size);
// s.delete(3)
// s.clear() // 清除s里的值
// console.log(s);


// const arr = [1, 2, 3, 4, 4, 3, 2, 1]
// const arr2 = [...new Set(arr)]

// console.log(arr2);


// const str = 'abcabc'
// console.log(new Set(str));

const s = new Set([1, 2, 3, 3])
// console.log(s.keys());
// console.log(s.values());
// console.log(s.entries());

// for (let item of s.entries()) { // 专门用来遍历具有iterable属性的结构
//     console.log(item);
// }

s.forEach((val, key) => {
    console.log(val, key);
})
