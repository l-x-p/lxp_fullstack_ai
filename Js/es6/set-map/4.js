// node的全局对象 gobal
global.gc() // 垃圾回收
// process.memoryUsage() // 2.6M

let obj = {name: 'lxp', age: new Array(5 * 1024 * 1024)}
let ws = new WeakSet()
ws.add(obj)

obj = null  // 触发垃圾回收
global.gc()
console.log(process.memoryUsage());  // 4.4M  2.5M