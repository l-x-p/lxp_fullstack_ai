const user = {
    name: {
        firstname: 'L',
        lastname: 'xp'
    },
    age: 18
}

function deep(obj) {
    let newObj = {}
    for(let key in obj) {
        if (obj.hasOwnProperty(key)){
            if(obj[key] instanceof Object) {  // obj[key] == 'object' && obj[key] !== null  obj[key] 是不是对象
                newObj[key] = deep(obj[key]);
            } else{
                newObj[key] = obj[key]
            }
        }
    }
    return newObj
}
const newobj = deep(user)
user.name.firstname = 'zhangsan'
console.log(newobj);