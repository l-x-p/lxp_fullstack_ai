let obj = {
    name: 'lxp',
    like: {
        a: 'food'
    }
}

function shallow(obj) {
    let newObj = {}
    for(let key in obj) {
        if(obj.hasOwnProperty(key)){
        newObj[key] = obj[key]
    }}
    return newObj
}
let obj2 = shallow(obj)
obj2.like.a = 'sport'
console.log(shallow(obj));