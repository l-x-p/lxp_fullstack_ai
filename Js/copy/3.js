let obj = {
    name: 'lxp',
    age: 18,
    like: {
        n: 'music'
    },
    a: true,
    b: undefined,
    c: null,
    d: Symbol(1),
    f: function() {}
}
obj.c = obj.like
obj.like.n = obj.c
console.log(obj);

let obj2 = JSON.parse(JSON.stringify(obj))

obj.like.n = 'majiang'
console.log(obj2);