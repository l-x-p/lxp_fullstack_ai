// let a = {
//     name: 'lxp'
// }

// let b = Object.create(a)
// a.name = 'lt'

// console.log(b.name);

// let a = {
//     name: 'lxp',
//     like: {
//         m: 'movie'
//     }
// }

// let b = {
//     age: 18
// }

// let c = Object.assign({}, a)
// a.like.m = 'music'
// console.log(c, a);

let arr = [1, 2, 3, {a: 10}]
// let newArr = [].concat(arr)  // 将arr中的元素和[]中的元素合并，并返回
let newArr = [...arr]

arr[3].a = 20
// arr.splice(1, 1)
console.log(newArr);