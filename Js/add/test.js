const curry = (fn, ...args) => 
    args.length >= fn.length
    ? fn(...args)
    :(..._args) => curry(fn, ...args, ..._args)
const add = (x, y, z, m) => {
    return x + y + z + m
}
console.log(curry(add, 1)(2)(3)(4));