// var obj = {}
// var arr = []
// var date = new Date() //获取当前时间


// Person.length // 得到函数参数的个数
// Person.prototype  函数的原型  {}  函数的祖先
Person.prototype.lastName = 'Li'
Person.prototype.say = function() {
    console.log('hello');
}
function Person() {
    this.name = 'xiaopeng'
}
let p1 = new Person() // 隐式具有 lastName say
console.log(p1.lastName);
